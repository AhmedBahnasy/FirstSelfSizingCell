//
//  KhatraCell.swift
//  KhatraCell
//
//  Created by Ahmed on 7/20/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit
import SDWebImage

class KhatraCell: UITableViewCell {

    @IBOutlet weak var dynamicLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

   
    var khatra: Khatra!
    
    func configureCell(khatra: Khatra) {

        self.khatra = khatra
        self.userName.text = khatra.userName
        self.dynamicLabel.text = khatra.text
        self.userImage.sd_setImage(with: URL(string: khatra.userImage), placeholderImage: UIImage(named : "ابوتريكة"))
        //self.userImage.image = #imageLiteral(resourceName: "ابوتريكة")
        self.dateLabel.text = khatra.date
        self.sideView.backgroundColor = Helper.getCategory(withID: khatra.categoryID)?.color
        self.userImage.setRounded()
        self.outerView.setCardView()
        self.sideView.layer.cornerRadius = 10
        //roundCorners(corners: [.topRight, .bottomRight], radius: 10)
    }
}
