//
//  CategoryCell.swift
//  FinalTest
//
//  Created by Ahmed on 7/25/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryCount: UILabel!
    @IBOutlet weak var outerView: UIView!
    var category: Category!
    
    func configureCell(category: Category) {
        
        self.category = category
        self.categoryName.text = category.name
        self.categoryCount.text = "\(category.count)"
        
        self.categoryName.textColor = category.color
        self.categoryCount.backgroundColor = category.color
        self.categoryCount.layer.cornerRadius = self.categoryCount.frame.width/2
        self.categoryCount.layer.masksToBounds = true
        self.categoryCount.numberOfLines = 0
        self.categoryCount.lineBreakMode = .byClipping
        self.categoryCount.adjustsFontSizeToFitWidth = true
        self.categoryCount.minimumScaleFactor = 0.5
        self.categoryCount.font = UIFont.systemFont(ofSize: 15)
        
        self.outerView.layer.cornerRadius = 20
        self.outerView.layer.masksToBounds = false
        self.outerView.layer.borderWidth = 1
        self.outerView.layer.borderColor = category.color.cgColor
    }
}
