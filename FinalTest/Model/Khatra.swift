//
//  Khatra.swift
//  FinalTest
//
//  Created by Ahmed on 8/1/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit
import SwiftyJSON

class Khatra {
    
    private var _id: Int!
    private var _userName: String!
    private var _userImage: String!
    private var _text: String!
    private var _date: String!
    private var _categoryID: Int!
    
    var id: Int? {
        return _id
    }
    var userName: String {
        return _userName
    }
    var userImage: String {
        return _userImage
    }
    var text: String {
        return _text
    }
    var date: String {
        return _date
    }
    var categoryID: Int {
        return _categoryID
    }
    
    init?(dict: [String : JSON]) {
        guard let id = dict["id"]?.int, let text = dict["text"]?.string,let date = dict["date"]?.int, let category = dict["category"]?.dictionary, let categoryID = category["id"]?.int, let user = dict["user"]?.dictionary, let userName = user["name"]?.string, let userImage = user["image"]?.string else { return nil }
    
        self._id = id
        self._userName = userName
        self._userImage = userImage
        self._text = text
        self._date = "\(date)"
        self._categoryID = categoryID
    }
}





