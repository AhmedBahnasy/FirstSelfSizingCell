//
//  Post.swift
//  FinalTest
//
//  Created by Ahmed on 7/24/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit

enum Categories {
    case Estaed
    case Arfa
    case Saai
    case Gamarat
    case Tawaf
    case Mena
}

class Post {
 
    private var _userName: String!
    private var _userPost: String!
    private var _userImage: String!
    private var _postCategory: Categories!
    private var _postColor: UIColor!
    
    var userName: String {
        return _userName
    }
    var userPost: String {
        return _userPost
    }
    var userImage: String {
        return _userImage
    }
    var postCategory: Categories {
        return _postCategory
    }
    var postColor: UIColor {
        return _postColor
    }
    
    init(userName: String, userPost: String, userImage: String, categoryID: Int) {
        self._userName = userName
        self._userPost = userPost
        self._userImage = userImage
        switch categoryID {
        case 0:
            self._postCategory = Categories.Estaed
            self._postColor = UIColor(hexString: "#148872")
        case 1:
            self._postCategory = Categories.Arfa
            self._postColor = UIColor(hexString: "#caa124")
        case 2:
            self._postCategory = Categories.Saai
            self._postColor = UIColor(hexString: "#289bd2")
        case 3:
            self._postCategory = Categories.Gamarat
            self._postColor = UIColor(hexString: "#d47a39")
        case 4:
            self._postCategory = Categories.Tawaf
            self._postColor = UIColor(hexString: "#1676a6")
        case 5:
            self._postCategory = Categories.Mena
            self._postColor = UIColor(hexString: "#899519")
        default:
            print("undefined categoryID")
        }
    }
}
