//
//  Category.swift
//  FinalTest
//
//  Created by Ahmed on 7/26/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit
import SwiftyJSON

class Category: NSObject, NSCoding {
    
    private var _id: Int?
    private var _name: String!
    private var _color: UIColor!
    private var _activeColor: UIColor!
    private var _count: Int!
    
    var id: Int {
        return _id!
    }
    var name: String {
        return _name
    }
    var color: UIColor {
        return _color
    }
    var activeColor: UIColor {
        return _activeColor
    }
    var count: Int {
        get {
            return _count
        } set {
            _count = newValue
        }
    }
    
    init(id: Int, name: String, count: Int, color: UIColor) {
        self._name = name
        self._count = count
        self._color = color
        self._id = id
    }
    required init?(coder aDecoder: NSCoder) {
        self._id = aDecoder.decodeObject(forKey: "id") as? Int
        self._name = aDecoder.decodeObject(forKey: "name") as! String
        self._color = aDecoder.decodeObject(forKey: "color") as! UIColor
        self._activeColor = aDecoder.decodeObject(forKey: "activeColor") as! UIColor
        self._count = aDecoder.decodeObject(forKey: "count") as! Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self._id, forKey: "id")
        aCoder.encode(self._name, forKey: "name")
        aCoder.encode(self._color, forKey: "color")
        aCoder.encode(self._activeColor, forKey: "activeColor")
        aCoder.encode(self._count, forKey: "count")
    }
    
    init?(dict : [String : JSON]) {
        guard let id = dict["id"]?.int, let name = dict["name"]?.string, let color = dict["color"]?.string, let activeColor = dict["activeColor"]?.string, let count = dict["count"]?.int else { return nil }
        
        self._id = id
        self._name = name
        self._color = UIColor(hexString: color)
        self._activeColor = UIColor(hexString: activeColor)
        self._count = count
    }
}
