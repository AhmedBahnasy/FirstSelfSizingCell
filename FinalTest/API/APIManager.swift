//
//  APIManager.swift
//  FinalTest
//
//  Created by Ahmed on 7/27/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APIManager: NSObject {
    
    static var categoriesCount = 0
    class func loadCategories(completion : @escaping (_ error : Error?, _ categories : [Category]?) -> Void) {
        
        let url = "http://api.mutawef.com/categories/index"
        let parameters: Parameters = ["timeStamp" : "1500292867"]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                switch response.result {
                case .failure(let error):
                    completion(error, nil)
                    print(error)
                    
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    guard let dataDict = json.dictionary, let dataArr = dataDict["categories"]?.array else{
                        completion(nil, nil)
                        return
                    }
                    var categoryArr = [Category(id: -1, name: "الكل", count: 0, color: UIColor(hexString: "#704C8B"))]
                    Helper.saveCategories(category: categoryArr[0])
                    
                    for data in dataArr {
                        if let data = data.dictionary, let category = Category.init(dict: data) {
                            categoriesCount+=category.count
                            categoryArr.append(category)
                            categoryArr[0].count = categoriesCount
                            Helper.saveCategories(category: category)
                            print(categoryArr)
                        }
                    }
                    completion(nil, categoryArr)
                }
        }
    }
    
    class func loadAllKhawater(khatraId: Int, count: Int, isNew: Bool, completion : @escaping (_ error : Error?, _ khawater : [Khatra]?) -> Void) {
        
        let url = "http://api.mutawef.com/khwater/index"
        let parameters: Parameters = [
            "khatraId" : "\(khatraId)",
            "count" : "\(count)",
            "isNew" : "\(isNew)"
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                switch response.result {
                case .failure(let error):
                    completion(error, nil)
                    print(error)
                    
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    guard let dataDict = json.dictionary, let dataArr = dataDict["khwater"]?.array else{
                        completion(nil, nil)
                        return
                    }
                    var khwaterArr = [Khatra]()
                    for data in dataArr {
                        if let data = data.dictionary, let khatra = Khatra.init(dict: data) {
                            khwaterArr.append(khatra)
                        }
                    }
                    completion(nil, khwaterArr)
                }
        }
    }
    
    class func loadSpecificKhawater(categoryID: Int, khatraId: Int, count: Int, isNew: Bool, completion : @escaping (_ error : Error?, _ khawater : [Khatra]?) -> Void) {
        
        let url = "http://api.mutawef.com/khwater/category"
        let parameters: Parameters = [
            "categoryId" : "\(categoryID)",
            "khatraId" : "\(khatraId)",
            "count" : "\(count)",
            "isNew" : "\(isNew)"
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                switch response.result {
                case .failure(let error):
                    completion(error, nil)
                    print(error)
                    
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    guard let dataDict = json.dictionary, let dataArr = dataDict["khwater"]?.array else{
                        completion(nil, nil)
                        return
                    }
                    var khwaterArr = [Khatra]()
                    for data in dataArr {
                        if let data = data.dictionary, let khatra = Khatra.init(dict: data) {
                            khwaterArr.append(khatra)
                        }
                    }
                    completion(nil, khwaterArr)
                }
        }
    }
}
