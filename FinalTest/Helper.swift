//
//  Helper.swift
//  FinalTest
//
//  Created by Ahmed on 8/2/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit

class Helper: NSObject {
    
    class func saveCategories(category: Category) {
        let data  = NSKeyedArchiver.archivedData(withRootObject: category)
        let defaults = UserDefaults.standard
        defaults.set(data, forKey:"\(category.id)")
        defaults.synchronize()
    }
    
    class func getCategory(withID: Int) -> Category? {
        if let data = UserDefaults.standard.object(forKey: "\(withID)") as? NSData {
            let category = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! Category
            return category
        }
        return nil
    }
}
