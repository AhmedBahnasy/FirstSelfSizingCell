//
//  UIImage+Extension.swift
//  FinalTest
//
//  Created by Ahmed on 7/24/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setRounded() {
        
        //self.layer.borderWidth = 0.1
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
}
