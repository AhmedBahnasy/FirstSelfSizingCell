//
//  UIView+Extension.swift
//  FinalTest
//
//  Created by Ahmed on 7/23/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit


extension UIView {
    
    func setCardView(){
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.cornerRadius = 10
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        //self.layer.borderWidth = 0.1
        //self.clipsToBounds = true
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
