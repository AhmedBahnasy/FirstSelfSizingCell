//
//  ViewController.swift
//  FinalTest
//
//  Created by Ahmed on 7/19/17.
//  Copyright © 2017 Ahmed. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    fileprivate let KhatraCellID = "KhatraCell"
    fileprivate let categoryCellID = "CategoryCell"
    var categoryArr = [Category]()
    var khwaterArr = [Khatra]()
    var isCategorySelected: Bool = false
    var isAllSelected: Bool = false
    var selectedCategory: Category!
    
    
    @IBOutlet weak var middleContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.middleContainerView.roundCorners(corners: [.allCorners], radius: 20)
        
        collectionView.register(UINib.init(nibName: categoryCellID, bundle: nil), forCellWithReuseIdentifier: categoryCellID)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = flowlayout
        loadCategories()
        
        
        tableView.register(UINib.init(nibName: KhatraCellID, bundle: nil), forCellReuseIdentifier: KhatraCellID)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        loadKhawater(khatraId: 0, isNew: true)
    }
    
    private func loadCategories() {
        APIManager.loadCategories{(error : Error?, categories : [Category]?) in
            if let categories = categories {
                self.categoryArr.append(contentsOf: categories)
                self.selectedCategory = self.categoryArr[0]
                self.collectionView.reloadData()
            }
        }
    }
    fileprivate func loadKhawater(khatraId: Int, isNew: Bool) {
        APIManager.loadAllKhawater(khatraId: khatraId, count: 10, isNew: isNew) {(error : Error?, khawater : [Khatra]?) in
            if let khawater = khawater {
                self.khwaterArr.append(contentsOf: khawater)
                self.tableView.reloadData()
            }
        }
    }
    fileprivate func loadCertainKhawter(categoryID: Int, khatraId: Int, isNew: Bool) {
        APIManager.loadSpecificKhawater(categoryID: categoryID, khatraId: khatraId, count: 3, isNew: isNew) {(error : Error?, khawater : [Khatra]?) in
            if let khawater = khawater {
                self.khwaterArr.append(contentsOf: khawater)
                self.tableView.reloadData()
            }
        }
    }
//    override func viewWillLayoutSubviews() {
//        self.flowlayout.itemSize = CGSize(width: (collectionView.frame.size.width) / 3.5, height: 40)
//        self.flowlayout.invalidateLayout()
//    }
//    override func viewDidLayoutSubviews() {
//        self.flowlayout.itemSize = CGSize(width: (collectionView.frame.size.width) / 3.5, height: 40)
//    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return khwaterArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: KhatraCellID, for: indexPath) as? KhatraCell else {
            return UITableViewCell() }
        cell.configureCell(khatra: khwaterArr[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let count = self.khwaterArr.count
        if indexPath.row == count-1 {
            if let khatraID = self.khwaterArr[indexPath.row].id {
                if !isCategorySelected || selectedCategory.id == -1 {
                    self.loadKhawater(khatraId: khatraID, isNew: false)
                } else {
                    let categoryID = selectedCategory.id
                    self.loadCertainKhawter(categoryID: categoryID, khatraId: khatraID, isNew: false)
                }
            }
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryCellID, for: indexPath) as? CategoryCell else { return UICollectionViewCell() }
        cell.configureCell(category: categoryArr[indexPath.row])
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width) / 3.5
        
        return CGSize(width: width, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.isCategorySelected = true
        self.khwaterArr = []
        if let cell = collectionView.cellForItem(at: indexPath) as? CategoryCell {
            selectedCategory = cell.category
            cell.outerView.backgroundColor = categoryArr[indexPath.row].color.lighter(by: 10)
            cell.categoryName.textColor = UIColor.white
        }
        if indexPath.row == 0 {
            //isAllSelected = true
            loadKhawater(khatraId: 0, isNew: true)
        }else {
            let categoryID = selectedCategory.id
            loadCertainKhawter(categoryID: categoryID, khatraId: 0, isNew: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CategoryCell {
            cell.outerView.backgroundColor = UIColor.white
            cell.categoryName.textColor = categoryArr[indexPath.row].color
        }
    }
}
